from flask import render_template, request
from flask_login import login_required
from datetime import timedelta
from core import app, db
from backend.models import QueueEvent, QueueActor, Queue, QueueType


@app.route('/queues', methods=['GET', 'POST'])
@login_required
def queues():
    if request.method == 'POST':
        queue_id = int(request.form.get('queue_id'))
        queue = Queue.query.filter_by(id=queue_id).first()

        if not queue:
            return render_template('queues/queues.html',
                                   msg_error="Queue doesn't exist",
                                   all_queues=Queue.query.all(),
                                   all_queue_actors=QueueActor.query.all(),
                                   all_queue_events=QueueEvent.query.all())
        name = queue.name
        db.session.delete(queue)
        db.session.commit()
        return render_template('queues/queues.html',
                               msg_success="Queue '" + name + "' successfully deleted",
                               all_queues=Queue.query.all(),
                               all_queue_actors=QueueActor.query.all(),
                               all_queue_events=QueueEvent.query.all())

    return render_template('queues/queues.html',
                           all_queues=Queue.query.all(),
                           all_queue_actors=QueueActor.query.all(),
                           all_queue_events=QueueEvent.query.all())


def get_queue_from_form():
    name = request.form.get('name')
    period_days = int(request.form.get('period_days'))
    period_hours = int(request.form.get('period_hours'))
    period_min = int(request.form.get('period_min'))
    period_sec = int(request.form.get('period_sec'))
    queue_type = QueueType(int(request.form.get('type')))
    comments = request.form.get('comments')
    period = timedelta(days=period_days, hours=period_hours, minutes=period_min, seconds=period_sec)

    return Queue(name=name, period=period, type=queue_type, comments=comments)


@app.route('/queue_create', methods=['GET', 'POST'])
@login_required
def queue_create():
    new_queue = Queue()

    if request.method == 'POST':
        new_queue = get_queue_from_form()

        if Queue.query.filter_by(name=new_queue.name).first():
            return render_template('queues/queue_create.html',
                                   queue=new_queue,
                                   msg_error="Queue with name '" + new_queue.name + "' already exist")

        db.session.add(new_queue)
        db.session.commit()

        return render_template('queues/queue_create.html',
                               queue=new_queue,
                               msg_success="Queue '" + new_queue.name + "' successfully created")

    return render_template('queues/queue_create.html', queue=new_queue)


@app.route('/queue_edit/<int:queue_id>', methods=['GET', 'POST'])
@login_required
def queue_edit(queue_id):
    edit_queue = Queue.query.filter_by(id=queue_id).first()

    if request.method == 'POST':
        new_queue = get_queue_from_form()

        found = Queue.query.filter_by(name=new_queue.name).first()
        if found is not None and found.id is not edit_queue.id:
            return render_template('queues/queue_edit.html',
                                   queue=new_queue,
                                   msg_error="Queue with name '" + new_queue.name + "' already exist")

        edit_queue.apply(new_queue)
        db.session.commit()

        return render_template('queues/queue_edit.html',
                               queue=edit_queue,
                               msg_success="Queue '" + new_queue.name + "' successfully saved")

    return render_template('queues/queue_edit.html', queue=edit_queue)


@app.route('/demo')
def demo():
    return render_template('queues/demo.html')
