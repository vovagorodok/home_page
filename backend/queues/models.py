from core import db
from datetime import timedelta
from sqlalchemy.sql import func
from enum import Enum


class QueueEventType(Enum):
    PASS = 1
    DELAY = 2
    IGNORE = 3


class QueueType(Enum):
    NOT_PERIODIC = 1
    PERIODIC = 2
    PERIODIC_CONST_INTERVALS = 3


class QueueEvent(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime, default=func.now(), onupdate=func.now())
    type = db.Column(db.Enum(QueueEventType), default=QueueEventType.PASS)
    comments = db.Column(db.String, nullable=True)
    actor_id = db.Column(db.Integer, db.ForeignKey('queue_actor.id'))
    queue_id = db.Column(db.Integer, db.ForeignKey('queue.id'))
    actor = db.relationship('QueueActor')
    queue = db.relationship('Queue')


class QueueActor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    order_num = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    queue_id = db.Column(db.Integer, db.ForeignKey('queue.id'))
    user = db.relationship('User')
    events = db.relationship('QueueEvent', back_populates='actor', order_by=QueueEvent.time)
    queue = db.relationship('Queue', back_populates='actors')


class Queue(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(15), unique=True)
    period = db.Column(db.Interval, default=timedelta(0))
    type = db.Column(db.Enum(QueueType), default=QueueType.NOT_PERIODIC)
    comments = db.Column(db.String, nullable=True)
    actors = db.relationship('QueueActor', back_populates='queue', order_by=QueueActor.order_num)
    events = db.relationship('QueueEvent', back_populates='queue', order_by=QueueEvent.time)

    def is_type_equals(self, queue_type):
        return self.type == QueueType(queue_type)

    def get_period_seconds(self):
        return (self.period.seconds % 3600) % 60

    def get_period_minutes(self):
        return (self.period.seconds % 3600) // 60

    def get_period_hours(self):
        return self.period.seconds // 3600

    def get_period_days(self):
        return self.period.days

    def __init__(self, name="", period=timedelta(0), type=QueueType.NOT_PERIODIC, comments=""):
        super().__init__()
        self.name = name
        self.period = period
        self.type = type
        self.comments = comments

    def apply(self, other):
        self.name = other.name
        self.period = other.period
        self.type = other.type
        self.comments = other.comments
