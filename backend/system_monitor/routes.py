from core import app
from flask import jsonify, render_template
from datetime import datetime
import platform
import cpuinfo
import psutil


def get_status_row(param, value):
    return param + "<b>" + value + "</b>"


class SystemInfo:
    B_IN_KB = 1024
    KB_IN_MB = 1024
    MB_IN_GB = 1024
    B_IN_MB = KB_IN_MB * B_IN_KB
    B_IN_GB = B_IN_MB * MB_IN_GB

    def __init__(self):
        self.json = dict()
        self.info_id = 0

    def add_item(self, param, value):
        self.json["sinfo-{:03d}".format(self.info_id)] = get_status_row(param, value)
        self.info_id += 1

    def add_line(self):
        self.add_item("", "\n")

    def add_sys(self):
        self.add_item("OS INFO:", "")
        self.add_item("  SYSTEM:  ", platform.system())
        self.add_item("  RELEASE: ", platform.release())
        self.add_item("  VERSION: ", platform.version())
        self.add_item("  MACHINE: ", platform.machine())
        self.add_item("  NODE:    ", platform.node())

    def add_cpu_load_per_core(self):
        for idx, val in enumerate(psutil.cpu_percent(percpu=True)):
            self.add_item("    CORE {}: ".format(idx), "{: 5.1f}%".format(val))

    def add_cpu_freq(self):
        current, min, max = psutil.cpu_freq()
        self.add_item("  FREQ:     ", "current: {: 6.1f}, min: {: 6.1f}, max: {: 6.1f}".format(current, min, max))

    def add_cpu(self):
        self.add_item("CPU INFO:", "")
        self.add_item("  BRAND:    ", cpuinfo.get_cpu_info()['brand'])
        self.add_item("  CORES:    ", "logical: {}, physical: {}".format(psutil.cpu_count(), psutil.cpu_count(logical=False)))
        self.add_cpu_freq()
        self.add_item("  LOAD:     ", "{: 5.1f}%".format(psutil.cpu_percent()))
        self.add_cpu_load_per_core()

    def add_ram(self):
        mem = psutil.virtual_memory()
        self.add_item("RAM INFO:", "")
        self.add_item("  USED:   ", "{: 5.1f}%".format(mem.percent))
        self.add_item("  TOTAL:  ", "{: 5.0f}MB".format(mem.total / self.B_IN_MB))
        self.add_item("  FREE:   ", "{: 5.0f}MB".format(mem.free / self.B_IN_MB))
        self.add_item("  CACHED: ", "{: 5.0f}MB".format(mem.cached / self.B_IN_MB))

    def add_swap(self):
        mem = psutil.swap_memory()
        self.add_item("SWAP INFO:", "")
        self.add_item("  USED:  ", "{: 5.1f}%".format(mem.percent))
        self.add_item("  TOTAL: ", "{: 5.0f}MB".format(mem.total / self.B_IN_MB))

    def add_disk(self):
        self.add_item("DISK INFO:", "")
        for disk in psutil.disk_partitions():
            if "loop" not in disk.device:
                usage = psutil.disk_usage(disk.mountpoint)
                self.add_item("  MOUNTPOINT: ", disk.mountpoint)
                self.add_item("    DEVIDE:   ", disk.device)
                self.add_item("    TYPE:     ", disk.fstype)
                self.add_item("    TOTAL:    ", "{: 6.1f}GB".format(usage.total / self.B_IN_GB))
                self.add_item("    USED:     ", "{: 6.1f}GB".format(usage.used / self.B_IN_GB))
                self.add_item("    FREE:     ", "{: 6.1f}GB".format(usage.free / self.B_IN_GB))

    def add_network(self):
        addresses = []
        counters = psutil.net_io_counters()

        self.add_item("NETWORK INFO:", "")
        for name, nets in psutil.net_if_addrs().items():
            for net in nets:
                if net.family == 2:
                    addresses.append("{}: {}".format(name, net.address))
        self.add_item("  ADDRESSES: ", ", ".join(addresses))
        self.add_item("  SENT: ", "{: 13.0f}B, {: 10.0f}PKGS".format(counters.bytes_sent, counters.packets_sent))
        self.add_item("  RECV: ", "{: 13.0f}B, {: 10.0f}PKGS".format(counters.bytes_recv, counters.packets_recv))

    def add_sensors(self):
        self.add_item("SENSORS INFO:", "")
        self.add_item("  TEMPERATURE:", "")
        for name, sens in psutil.sensors_temperatures().items():
            self.add_item("    GROUP: ", name)
            for sen in sens:
                info = "label: {:16} current: {: 6.1f}℃, high: {: 6.1f}℃, critical: {: 6.1f}℃".format(
                    "{},".format(sen.label),
                    sen.current or float('nan'),
                    sen.high or float('nan'),
                    sen.critical or float('nan')
                )
                self.add_item("      SENSOR: ", info)

    def add_time(self):
        current_time = datetime.now()
        boot_time = datetime.fromtimestamp(psutil.boot_time())
        current_time_str = current_time.strftime("%Y-%m-%d %H:%M:%S")
        boot_time_str = boot_time.strftime("%Y-%m-%d %H:%M:%S")
        power_on_time_str = str(current_time - boot_time)

        self.add_item("TIME INFO:", "")
        self.add_item("  CURRENT:  ", current_time_str)
        self.add_item("  BOOT:     ", boot_time_str)
        self.add_item("  POWER ON: ", power_on_time_str)

    def generate_info(self):
        adders = [self.add_sys,
                  self.add_cpu,
                  self.add_ram,
                  self.add_swap,
                  self.add_disk,
                  self.add_network,
                  self.add_sensors,
                  self.add_time]
        for adder in adders:
            try:
                adder()
            except:
                None

        return self.json


@app.route('/monitor/status.json')
def monitor_status():
    info = SystemInfo().generate_info()
    return jsonify(info)


@app.route('/monitor')
def monitor():
    return render_template("/system_monitor/monitor.html")
