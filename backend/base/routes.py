from flask import render_template
from core import app


def get_error_page(error_code, error_description):
    return render_template(
        'base/error.html', error_code=error_code, msg_error=error_description), error_code


@app.errorhandler(404)
def page_not_found(e):
    return get_error_page(404, "Page not found")


@app.errorhandler(500)
def internal_server_error(e):
    return get_error_page(500, "Internal server error")


@app.route("/")
@app.route('/index')
def index():
    return render_template("base/index.html")
