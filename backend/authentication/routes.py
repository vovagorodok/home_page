from flask import render_template, redirect, url_for, request, current_app
from flask_login import login_user, login_required, logout_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from functools import wraps
from core import app, db, login_manager
from backend.models import User, UserRole


def login_as_admin_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if current_user.is_authenticated_as_admin():
            return func(*args, **kwargs)
        return current_app.login_manager.unauthorized()
    return decorated_view


def get_user_from_register_form():
    username = request.form.get('username')
    password = request.form.get('password')
    email = request.form.get('email')
    hashed_password = generate_password_hash(password, method='sha256')

    return User(username=username, email=email, password=hashed_password)


def get_user_from_edit_form():
    username = request.form.get('username')
    email = request.form.get('email')
    role = UserRole(int(request.form.get('role')))

    return User(username=username, email=email, role=role)


def get_user_from_change_password_form():
    password = request.form.get('password')
    hashed_password = generate_password_hash(password, method='sha256')

    return User(password=hashed_password)


def get_user_from_create_form():
    username = request.form.get('username')
    password = request.form.get('password')
    email = request.form.get('email')
    role = UserRole(int(request.form.get('role')))
    hashed_password = generate_password_hash(password, method='sha256')

    return User(username=username, email=email, password=hashed_password, role=role)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        remember = request.form.get('remember', default=False)

        user = User.query.filter_by(username=username).first()
        if user and check_password_hash(user.password, password):
            login_user(user, remember=remember)
            return redirect(url_for('users'))
        return render_template('authentication/login.html', msg_error="Invalid username or password")

    return render_template('authentication/login.html')


@app.route('/register', methods=['GET', 'POST'])
def register():
    new_user = User()

    if request.method == 'POST':
        new_user = get_user_from_register_form()

        if User.query.filter_by(username=new_user.username).first():
            return render_template('authentication/register.html',
                                   user=new_user,
                                   msg_error="User with username '" + new_user.username + "' already exist")
        if User.query.filter_by(email=new_user.email).first():
            return render_template('authentication/register.html',
                                   user=new_user,
                                   msg_error="User with email '" + new_user.email + "' already exist")

        db.session.add(new_user)
        db.session.commit()

        return render_template('authentication/register.html',
                               user=new_user,
                               msg_success="User '" + new_user.username + "' successfully created")

    return render_template('authentication/register.html', user=new_user)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/users')
@login_required
def users():
    return render_template('authentication/users.html',
                           all_users=User.query.all())


@app.route('/user_edit/<int:user_id>', methods=['GET', 'POST'])
@login_required
def user_edit(user_id):
    edit_user = User.query.filter_by(id=user_id).first()

    if request.method == 'POST':
        new_user = get_user_from_edit_form()
        new_user.id = edit_user.id

        found = User.query.filter_by(username=new_user.username).first()
        if found is not None and found.id is not edit_user.id:
            return render_template('authentication/user_edit.html',
                                   user=new_user,
                                   msg_error="User with username '" + new_user.username + "' already exist")

        found = User.query.filter_by(email=new_user.email).first()
        if found is not None and found.id is not edit_user.id:
            return render_template('authentication/user_edit.html',
                                   user=new_user,
                                   msg_error="User with email '" + new_user.email + "' already exist")

        edit_user.apply_without_password(new_user)
        db.session.commit()

        return render_template('authentication/user_edit.html',
                               user=edit_user,
                               msg_success="User '" + new_user.username + "' successfully saved")

    return render_template('authentication/user_edit.html', user=edit_user)


@app.route('/user_create', methods=['GET', 'POST'])
def user_create():
    new_user = User()

    if request.method == 'POST':
        new_user = get_user_from_create_form()

        if User.query.filter_by(username=new_user.username).first():
            return render_template('authentication/user_create.html',
                                   user=new_user,
                                   msg_error="User with username '" + new_user.username + "' already exist")
        if User.query.filter_by(email=new_user.email).first():
            return render_template('authentication/user_create.html',
                                   user=new_user,
                                   msg_error="User with email '" + new_user.email + "' already exist")

        db.session.add(new_user)
        db.session.commit()

        return render_template('authentication/user_create.html',
                               user=new_user,
                               msg_success="User '" + new_user.username + "' successfully created")

    return render_template('authentication/user_create.html', user=new_user)


@app.route('/user_delete/<int:user_id>')
@login_required
def user_delete(user_id):
    user = User.query.filter_by(id=user_id).first()

    if not user:
        return render_template('authentication/users.html',
                               msg_error="User doesn't exist",
                               all_users=User.query.all())

    db.session.delete(user)
    db.session.commit()

    return render_template('authentication/users.html',
                           msg_success="User '" + user.username + "' successfully deleted",
                           all_users=User.query.all())


@app.route('/user_change_password/<int:user_id>', methods=['GET', 'POST'])
@login_required
def user_change_password(user_id):
    user = User.query.filter_by(id=user_id).first()

    if request.method == 'POST':
        new_user = get_user_from_change_password_form()

        user.apply_password(new_user)
        db.session.commit()

        return render_template('authentication/user_change_password.html',
                               user=user,
                               msg_success="Password for '" + user.username + "' successfully changed")

    return render_template('authentication/user_change_password.html', user=user)
