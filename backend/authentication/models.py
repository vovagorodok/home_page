from core import db
from enum import Enum
from flask_login import UserMixin


class UserRole(Enum):
    USER = 1
    ADMINISTRATOR = 2


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15), unique=True)
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(80))
    role = db.Column(db.Enum(UserRole), default=UserRole.USER)

    def is_authenticated_as_admin(self):
        return self.is_authenticated and self.role is UserRole.ADMINISTRATOR

    def is_role_equals(self, role):
        return self.role == UserRole(role)

    def __init__(self, username="", email="", password="", role=UserRole.USER):
        super().__init__()
        self.username = username
        self.email = email
        self.password = password
        self.role = role

    def apply_without_password(self, other):
        self.username = other.username
        self.email = other.email
        self.role = other.role

    def apply_password(self, other):
        self.password = other.password

    def apply(self, other):
        self.apply_without_password(other)
        self.apply_password(other)
