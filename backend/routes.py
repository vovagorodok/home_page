# noinspection PyUnresolvedReferences
from backend.authentication.routes import load_user, login, register, logout, users, user_edit
# noinspection PyUnresolvedReferences
from backend.base.routes import page_not_found, internal_server_error, index
# noinspection PyUnresolvedReferences
from backend.system_monitor.routes import monitor_status, monitor
# noinspection PyUnresolvedReferences
from backend.queues.routes import queues, queue_create
# noinspection PyUnresolvedReferences
from backend.reader.routes import csv_reader, txt_reader
