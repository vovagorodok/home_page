from core import app
from flask import render_template, Response
import os
import csv
import random


def _get_path(path):
    if len(path) and path[0] == '@' and os.path.exists(path[1:]):
        return path[1:]
    return None


def _gen_rgb():
    return '{},{},{}'.format(random.randint(0, 255),
                             random.randint(0, 255),
                             random.randint(0, 255))


@app.route('/csv_reader/', defaults={'path': ''})
@app.route('/csv_reader/<path:path>')
def csv_reader(path):
    path = _get_path(path)
    if path is None:
        return Response("Incorrect path !!!", mimetype='text/plain')
    with open(path, 'r') as read_file:
        reader = csv.DictReader(read_file)
        return render_template('reader/reader.html', reader=reader)


@app.route('/txt_reader/', defaults={'path': ''})
@app.route('/txt_reader/<path:path>')
def txt_reader(path):
    path = _get_path(path)
    if path is None:
        return Response("Incorrect path !!!", mimetype='text/plain')
    with open(path, 'r') as read_file:
        return Response(read_file.readlines(), mimetype='text/plain')


@app.route('/line_chart/', defaults={'path': ''})
@app.route('/line_chart/<path:path>')
def line_chart(path):
    path = _get_path(path)
    if path is None:
        return Response("Incorrect path !!!", mimetype='text/plain')
    with open(path, 'r') as read_file:
        reader = csv.DictReader(read_file)
        fieldnames = reader.fieldnames
        reader = list(reader)
        colors = dict()
        for field_name in fieldnames:
            colors[field_name] = _gen_rgb()
        return render_template('reader/line_chart.html', fieldnames=fieldnames, reader=reader, colors=colors)


@app.route('/line_chart_percent_dif/', defaults={'path': ''})
@app.route('/line_chart_percent_dif/<path:path>')
def line_chart_percent_dif(path):
    path = _get_path(path)
    if path is None:
        return Response("Incorrect path !!!", mimetype='text/plain')
    with open(path, 'r') as read_file:
        reader = csv.DictReader(read_file)
        fieldnames = reader.fieldnames
        reader = list(reader)
        colors = dict()
        for field_name in fieldnames:
            colors[field_name] = _gen_rgb()
        for field_name in fieldnames[1:]:
            for row in reader[1:]:
                row[field_name] = ((float(row[field_name]) / float(reader[0][field_name])) - 1.0) * 100.0
            reader[0][field_name] = 0.0
        return render_template('reader/line_chart.html', fieldnames=fieldnames, reader=reader, colors=colors)
