#!/usr/bin/python3
from gevent.pywsgi import WSGIServer
from core import app
# noinspection PyUnresolvedReferences
from backend.routes import *


if __name__ == "__main__":
    if app.debug:
        print("Runing flask server ...")
        app.run(debug=True, host='0.0.0.0', port=5000)
    else:
        print("Runung wsgi server ...")
        http_server = WSGIServer(('', 5000), app)
        http_server.serve_forever()
