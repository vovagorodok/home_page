#!/usr/bin/python3
import pip


def install(package):
    pip.main(['install', package])


def install_packages():
    install('flask')
    install('flask_login')
    install('flask_sqlalchemy')
    install('werkzeug')
    install('gevent')
    install('py-cpuinfo')
    install('psutil')


if __name__ == '__main__':
    install_packages()
    # noinspection PyUnresolvedReferences
    from backend.models import *
    from core import db
    db.create_all()
