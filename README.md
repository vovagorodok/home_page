<h3 align="center">Home Page</h3>

<p align="center">
  Simple page with database and materialize design.
  Project use flask framework with materialize css stylesheets.
</p>


## Table of Contents
- [Quickstart](#quickstart)
- [Project structure](#project-structure)
- [Add new service](#add-new-service)

## Quickstart:
Project requires Python 3. Run server using following commands:

```bash
git clone https://gitlab.com/vovagorodok/home_page.git
cd home_page
python3 setup.py
python3 run.py &
```

Open page <http://localhost:5000> from your browser.

## Project structure:
A typical top-level directory layout:
<pre><code>.
├── backend/                # ORM objects declarations and route functions
├── static/                 # Materializecss stylesheets. CSS and JS files
├── templates/              # HTML templates
├── core.py                 # Core aplication objects
├── setup.py                # Installs libraries and creates database structure
├── run.py                  # Runs HTTP server
├── .gitignore
└── README.md
</code></pre>

## Add new service:
In order to create new_service please create following structure:
<pre><code>.
├── backend/
:   ├── new_service/          # Create backend for new_service
:   :
:   ├── models.py             # Add imports of new_service models here
:   └── routes.py             # Add imports of new_service routes here
└── templates/ 
    ├── new_service/          # Create HTML templates for new_service
    :
    └── base/
        └── nav_content.html  # Add reference to new_service here
</code></pre>

## Links:
No-Ip setup:

<http://sizious.com/2017/04/30/how-to-properly-install-no-ip-dynamic-update-client-duc-under-raspbian>

<http://www.noip.com/support/knowledgebase/install-ip-duc-onto-raspberry-pi>

<http://www.noip.com/support/knowledgebase/installing-the-linux-dynamic-update-client>