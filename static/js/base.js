$('.dropdown-trigger').dropdown();

$(document).ready(function(){
  $('select').formSelect();
});

$(document).ready(function(){
  $('.modal').modal();
});

var HtmlAutoUpdateTemplate = `<div id="{id}">{body}</div>`;

$( ".html-auto-update" ).each(function(index) {
  id = $(this).attr('id')
  url_json = $(this).attr('url_json')
  update_interval_msec = $(this).attr('update_interval_msec');
  updater = new ElementsUpdater(id, url_json, update_interval_msec, HtmlAutoUpdateTemplate);
});

var CardPanelAutoUpdateTemplate = `
  <div class="card-panel" id="{id}">
    <span class="blue-text text-darken-2">{body}</span>
  </div>
`;

$( ".card-panel-auto-update" ).each(function(index) {
  id = $(this).attr('id')
  url_json = $(this).attr('url_json')
  update_interval_msec = $(this).attr('update_interval_msec');
  updater = new ElementsUpdater(id, url_json, update_interval_msec, CardPanelAutoUpdateTemplate);
});