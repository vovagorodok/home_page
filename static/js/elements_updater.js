class ElementsUpdater {

  constructor(containerID, urlJson, updateIntervalMsec, elementTemplate)
  {
    this.urlJson = urlJson
    this.containerID = containerID
    this.elementTemplate = elementTemplate

    this.tick();
    self = this
    setInterval(function() {
        self.tick();
    }, updateIntervalMsec);
  }

  buildElement(id, body)
  {
    let r = this.elementTemplate
    r=r.replace(/{id}/g,   id);
    r=r.replace(/{body}/g, body);
    return r
  }

  createOrUpdate(id, val)
  {
    var elementAsHtml = this.buildElement(id, val);

    if ($("#" + id).length)
    {
      $("#" + id).replaceWith(elementAsHtml);
    } else {
      $("#" + this.containerID).append(elementAsHtml);
    }
  }

  tick()
  {
    self = this
    $.getJSON(self.urlJson, function(data) {
      $.each(data, function(key, val) {
        self.createOrUpdate(key, val);
      });
    })
    .fail(function() {
        console.log("ElementsUpdater: update error");
    })
  }

};